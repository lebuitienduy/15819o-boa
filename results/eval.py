import sys
import os,math

def rounding(x,n):
	ans=str(round(x,n))
	l= len(ans[ans.find('.')+1:])
	if l <n:
		ans = ans+'0'*(n-l)
	return ans

def parse_file(f):
	lines = [l.strip().split(' ')[-2:] for l in open(f,'r') if l.find('out[] =')>=0]
	return [float(e[1]) for e in lines],lines
	
def eval(arr):
	n=len(arr)
	arr= sorted(arr)
	ans ={}
	ans['max']=max(arr)
	ans['min']=min(arr)
	ans['mean']=float(sum(arr))/len(arr)
	ans['num']=len(arr)
	ans['median']=(arr[n/2]+arr[n/2-1])/2.0 if n%2==0 else arr[n/2]
	return ans
	
def eval2(n,arr):

	arr= sorted(arr)
	ans ={}
	avg=float(sum(arr))/len(arr)
	ans['mean']=avg 
	ans['stddev']=math.sqrt(sum([ (e-avg)**2 for e in arr])/len(arr))
	ans['%p']=float(len(arr))/n*100.0

	return ans

def parse_project_list(f):
	lines = [l.strip().split(' ')[-2] for l  in open(f,'r') if l.find('out[]')>=0]
	return set(lines)
	

indir = sys.argv[1]

info={}

rule_mp={'_buffered_io':'\#1', '_tail_recursion':'\#2',
'_new_instances':'\#5','_string_concatenation_loops':'\#3','_get_linkedlist':'\#4', '_no_sleep_bugs':'\#6'}
data_mp={'GitHub':'GitHub','SF':'SourceForge','Overall':'Overall'}

java_project_size={'GitHub':274989,'SourceForge':31432,'Overall':307544}
android_project_size={'GitHub':66093,'SourceForge':960,'Overall':76077}

android_list={}

for f in os.listdir(indir):
	if f.startswith('GitHub')==False and f.startswith('SF')==False:
		continue
	fpath = indir+'/'+f
	rule= f[f.find('_'):f.rfind('.')]
	data = f[:f.find('_')]
	if rule == '_android_list_full':
		android_list[data]=parse_project_list(fpath)
		continue
	counts = parse_file(fpath)
	if rule not in info:
		info[rule]=[]
	info[rule]+=[[data,counts[0],counts[1]]]
print android_list.keys()
for k in info:
	lst= info[k]
	for tp in lst:
		if tp[0] in android_list:
			plist = android_list[tp[0]]
			
			tp[2]= [float(x[1]) for x in tp[2] if x[0] in plist]
		else:
			print tp[0]
	
for k in info:
	
	info[k]+=[('Overall',[x for t in info[k] for x in t[1] ],[x for t in info[k] for x in t[2] ])]

for r in sorted(rule_mp.items(),key = lambda x: x[1]):
	rname = r[0]
	print '\\multirow{3}{*}{'+r[1]+'}'
	for e in info[r[0]]:
		data=data_mp[e[0]]
		arr=['',data if data!='Overall'else '\\textbf{Overall}']
		result = eval2(java_project_size[data],e[1])
		for m in ['%p','mean','stddev']:
			arr+=[(rounding(result[m],2))  if m !='%p' else (rounding(result[m],2))+'\%']
		print '&'.join(arr)+'\\\\'
	print '\\hline'
	print

print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
print 
	
for r in sorted(rule_mp.items(),key = lambda x: x[1]):
	rname = r[0]
	print '\\multirow{3}{*}{'+r[1]+'}'
	for e in info[r[0]]:
		data=data_mp[e[0]]
		arr=['',data if data!='Overall'else '\\textbf{Overall}']
		
		result = eval2(android_project_size[data],e[2])
		for m in ['%p','mean','stddev']:
			arr+=[(rounding(result[m],2))  if m !='%p' else (rounding(result[m],2))+'\%']
		print '&'.join(arr)+'\\\\'
	print '\\hline'
	print
		